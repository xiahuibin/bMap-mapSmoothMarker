var express = require('express');
var router = express.Router();
var multipart = require('connect-multiparty');
var fs = require('fs');
let getClientIp = function (req) {
  return req.headers['x-forwarded-for'] ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress ||
    req.connection.socket.remoteAddress || '';
};

/* GET home page. */
router.get('/', function (req, res, next) {
  /*
  try {
    fs.readdir('./public/images', function (err, files) {
      console.log(files.length);
      res.render('index', { title: 'Express', img: files[0] });
    });
  } catch (error) {
    res.render('index', { title: 'Express',img:getClientIp(req) });
  }*/
  res.render('index', { title: 'Express', img: getClientIp(req) });
});

var multipartMiddleware = multipart();
router.post('/file', multipartMiddleware, function (req, res, next) {
  console.log(`用户->${getClientIp(req)}`);
  console.log(`------------------------`);
  try {
    // 获得文件的临时路径
    var tmp_path = req.files.file.path;
    // 指定文件上传后的目录 - 示例为"images"目录。 
    // var target_path = './public/images/' + req.files.thumbnail.name;
    var target_path = './public/images/' + getClientIp(req);
    // 移动文件
    fs.rename(tmp_path, target_path, function (err) {
      if (err) throw err;
      // 删除临时文件夹文件, 
      fs.unlink(tmp_path, function () {
        if (err) throw err;
        console.log(`SUCCESS=>${target_path}`);
        res.json({ url: target_path.replace("./public",""), size: req.files.file.size, success: true });
      });
    });
  } catch (error) {
    console.log(`ERROR${error}`);
    res.json({ error: error, success: false });
  }
});

module.exports = router;
