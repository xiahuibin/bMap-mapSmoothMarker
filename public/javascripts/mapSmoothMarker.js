var markerTranslation = function () {
    var intervalFlag = "";
    /**
     * @description 开始移动
     * @param {Point} prvePoint 开始坐标点
     * @param {Point} newPoint 结束坐标点
     * @param {marker} marker 标注
     * @param {number} trme 完成动画秒数
     * @param {function} changeFun 每次移动触发的事件
     */
    var moveCar = function (prvePoint, newPoint, marker, trme, changeFun) {
        var _prvePoint = new BMap.Pixel(0, 0);
        var _newPoint = new BMap.Pixel(0, 0);
        //当前帧数
        var currentCount = 0;
        //初始坐标
        _prvePoint = map.getMapType().getProjection().lngLatToPoint(prvePoint);
        //获取结束点的(x,y)坐标
        _newPoint = map.getMapType().getProjection().lngLatToPoint(newPoint);
        //默认速度 米/秒
        intervalFlag ? clearInterval(intervalFlag) : "";
        count = Math.round(trme ? (trme / 100) : 50);//移动500次-5000毫秒
        // console.log(_getDistance(_prvePoint, _newPoint), "<==每秒应该跳动", count);
        //两点之间匀速移动
        intervalFlag = setInterval(function () {
            //两点之间当前帧数大于总帧数的时候，则说明已经完成移动
            if (currentCount >= count) {
                clearInterval(intervalFlag);
            } else {
                //动画移动
                currentCount++;//计数
                var x = linear(_prvePoint.x, _newPoint.x, currentCount, count);
                var y = linear(_prvePoint.y, _newPoint.y, currentCount, count);
                //根据平面坐标转化为球面坐标
                var pos = map.getMapType().getProjection().pointToLngLat(new BMap.Pixel(x, y));
                marker.setPosition(pos);
                changeFun(pos, marker, x, y);
                //调整方向
                setRotation(prvePoint, newPoint, marker);
            }
        }, 100);
    };
    //计算两点间的距离
    var _getDistance = function (pxA, pxB) {
        return Math.sqrt(Math.pow(pxA.x - pxB.x, 2) + Math.pow(pxA.y - pxB.y, 2));
    };
    //计算平移的下一个点
    var linear = function (initPos, targetPos, currentCount, count) {
        var b = initPos, c = targetPos - initPos, t = currentCount, d = count;
        return c * t / d + b;
    };
    //取的一段坐标内当中离自己最小的距离-单位（米）
    var getLineMin = function (arr, pos1) {
        var mi = [];
        arr.forEach(function (val) {//计算距离是否在规划路线
            var news = map.getMapType().getProjection().lngLatToPoint(pos1);
            var vals = map.getMapType().getProjection().lngLatToPoint(val);
            mi.push(_getDistance(news, vals));
        });
        var minNum = Infinity;//无穷大
        mi.forEach(function (val) {
            if (minNum > val) {
                minNum=val;
            };
        });
        return minNum==Infinity?0:minNum;
    };
    //设置方向
    var setRotation = function (curPos, targetPos, marker) {
        var deg = 0;
        curPos = map.pointToPixel(curPos);
        targetPos = map.pointToPixel(targetPos);
        if (targetPos.x != curPos.x) {
            var tan = (targetPos.y - curPos.y) / (targetPos.x - curPos.x), atan = Math
                .atan(tan);
            deg = atan * 360 / (2 * Math.PI);
            if (targetPos.x < curPos.x) {
                deg = -deg + 90 + 90;
            } else {
                deg = -deg;
            }
            marker.setRotation(-deg);
        } else {
            var disy = targetPos.y - curPos.y;
            var bias = 0;
            if (disy > 0) {
                bias = -1;
            }
            else {
                bias = 1;
            }
            marker.setRotation(-bias * 90);
        }
        return;
    };
    return {
        startCar: moveCar,
        getLineMin: getLineMin
    }
};